#include "example3.h"

int example3() {
    float a, h, S;

    std::cout << "Input a and h: " << std::endl;
    std::cin >> a >> h;    //введення даних із клавіатури

    S = a * h / 2;
    std::cout << " S = " << S << std::endl;

    return 0;
}