#include "string"
#include "utils.h"
#include "tasks.h"

typedef void (*TaskFunction)();

int main() {
    TaskFunction taskFunctions[] = {
            firstTask,
            secondTask
    };
    const std::string welcomeMessage = "This is Lab #7";

    print(welcomeMessage);
    print(generateLine((int) welcomeMessage.size()));

    bool isReady = false;
    while (!isReady) {
        int a = taskUAChooser(2);
        try {
            taskFunctions[a - 1]();
        } catch (RuntimeException &e) {
            printWithSpace("There was an Exception:", e.what());
        }
        const std::string message = "Enter any key to continue or 'q' to quit:";
        print(message);
        print(generateLine((int) message.length()));
        isReady = exitFromCircle(message);
    }


    return 0;
}
