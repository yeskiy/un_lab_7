#include "example1.h"
int example1(){
    int a, b;
    b=10;
    a=b++;
    std::cout << "a = " << a << "  b = " << b << std::endl;

    b = 10;
    a = ++b;
    std::cout << "a = " << a << "  b = " << b << std::endl;

    return 0;
}