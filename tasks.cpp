#include "tasks.h"
#include "cmath"

void firstTask() {
    const int radius = requireInt("Type Radius: ");
    const int height = requireInt("Type Height: ");
    printWithSpace("The volume of the cone is", (M_PI * pow(radius, 2) * height)/3);
};

void secondTask() {
    const int inputMinutes = requireInt("Type minutes to calc: ");

    int minutes = inputMinutes % 60;
    int hours = inputMinutes / 60;
    std::string hourQuantityWord = "hours";
    std::string minuteQuantityWord = "minutes";

    if (hours == 1) {
        hourQuantityWord = "hour";
    }

    if (minutes == 0) {
        printWithSpace("There was", hours, hourQuantityWord + "!");
    } else {

        if (minutes == 1) {
            minuteQuantityWord = "minute";
        }

        printWithSpace("There was", hours, hourQuantityWord, "and", minutes, minuteQuantityWord + "!");
    }
};