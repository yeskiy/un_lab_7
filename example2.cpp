#include "example2.h"

int example2() {
    int a, b;
    float a1, b1;

    a = 5;
    b = 3;
    std::cout << "a / b = " << a / b << std::endl << "a % b = " << a % b << std::endl;

    a1 = 5;
    b1 = 3;
    std::cout << "a1 / b1 = " << a1 / b1 << std::endl;

    return 0;
}