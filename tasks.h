//
// Created by eqwer on 08.11.2021.
//

#ifndef UN_LAB_7_TASKS_H
#define UN_LAB_7_TASKS_H

#include "utils.h"

void firstTask();
void secondTask();

#endif //UN_LAB_7_TASKS_H
